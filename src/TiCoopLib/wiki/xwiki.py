#!/usr/bin/python

#Version 1.1 - 2020-06-04 - Cyril CARGOU - Ajout des fonctions de gestion des utilisateurs
#https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/XWikiRESTfulAPI

#import du service de gestion des chemins 
import os 

#------------- Variables ------------------
#upload sur le wiki
wikiUser = 'TBD'
wikiPwd = 'TBD'
baseURL = 'TBD'

 #-- Upload du fichier vers le wiki
def uploadFile (filePath,url):
    global wikiUser, wikiPwd
    ##problème avec l'upload ne python, le script ajout des données au fichier.
    #test fait en lecture fchier r  et rb
    #logger.info('Upload du fchier sur le wiki TiCoop > Presentation > Membres des commissions...')
    #url = 'https://WIKI.BASE.URL/rest/wikis/xwiki/spaces/TiCoop/spaces/Presentation/spaces/Membres%20des%20commissions/pages/WebHome/attachments/listeMembre.json'
    #files = {'file': ('listeMembre.json', open(outFile, 'r').readlines() ,'application/json')}
    #response = requests.put(url, files=files,headers={"Content-Type": "application/json"},auth=HTTPBasicAuth(uploadUser,uploadPwd))
    stdin = os.popen("curl -L -s -u "+wikiUser+":"+wikiPwd+" -o /dev/null -w '%{http_code}' -X PUT --data-binary '@"+filePath+"' -H 'Content-Type: application/xml' "+url)
    
    out = stdin.read()
    if out == "201":
        #logger.info('Upload fait - ajoute du fichier')
        return 1
    if out == "202":
        #logger.info('Upload fait - mise à jour du fichier')
        return 2
    raise Exception('Upload en echec, code retour : ' + out)
    # si code 412, vérifier que la page wiki cible n'est pas en cours d'édition
  
