from .erp_api import ErpApi
from .dolibarr_api import DolibarrApi
from .odoo_api import OdooApi

from datetime import datetime

class Erp():
    
    def __init__(
        self,
        erp_name:str,
        erp_base_url:str,
        erp_api_token:str           = None,
        erp_username:str            = None,
        erp_password:str            = None,
        default_get_limit:int       = None,
        default_sort_order:str      = 'ASC',
        default_begin_date:datetime = datetime(1970,1,1,0,0,0),
        default_end_date:datetime   = datetime(2970,1,1,0,0,0)
    ):

        self.erp_name = erp_name.lower()
        
        if self.erp_name == 'dolibarr':
            self.api = DolibarrApi(
                erp_base_url,
                erp_api_token,
                erp_username,
                erp_password,
                default_get_limit,
                default_sort_order,
                default_begin_date,
                default_end_date,
            )
        
        elif self.erp_name == 'odoo':
            self.api = OdooApi(
                erp_base_url,
                erp_api_token,
                erp_username,
                erp_password,
                default_get_limit,
                default_sort_order,
                default_begin_date,
                default_end_date,
            )
        
        else:
            raise AttributeError("Invalid erp_name, must be Dolibarr or Odoo (no one else is implemented)")