#!/usr/bin/python

#import du service de log pour tracer l'action du script
import logging
#import du service de gestion des chemins 
import os 
#import du service client REST API
import requests
from requests.auth import HTTPBasicAuth
import json
#pour la gestion des accents
import unicodedata
#pour l'export en csv
import csv
#pour api xmlrpc de vracoop
import xmlrpc.client
#pour le filtre des articles modifiés
import datetime
#pour la gestion des arguments
import sys, getopt
#pour la gestion des regex
import re

#------------ Variables
urlCaisse = "https://ticoop.vracoop.fr"
db = "todo"
username = 'todo'
password = "todo"

savedCurrentUid=-1

#varraible gloale pour réduire le nombre de requete vers le serveur
globalCacheHashLedger = {}

#------------ function
def getDicTVA():
    #out=caisse.execute_kw(db, uid, password,  'account.tax', 'search_read',{},{'fields': ['id', 'name']})
    #ou directement dans le code de la page Odoo, partie vente
    # #print(out)
    #{'id': 42, 'name': 'TVA 0% autres opérations non imposables (vente)'},
    #{'id': 9, 'name': 'TVA 20,0% TTC collectée (vente)'},
    #{'id': 12, 'name': 'TVA 10,0% TTC collectée (vente)'},
    #{'id': 13, 'name': 'TVA 5,5% TTC collectée (vente)'}, 
    #{'id': 15, 'name': 'TVA collectée (vente) 2,1% TTC'}, 
    return {
        '0.000' : 42,
        '2.100' : 15,
        '5.500' : 13,
        '10.000' : 12,
        '20.000' : 9,
    }

def getDicSupplierTVA():
    #olbigatoire car la tva d'acha défini la tva de vente ... (config par défaut de Vracoop, peut être changé en admin sur Odoo)
    #ou directement dans le code de la page Odoo, partie achats
    #{'id': 45, 'name': 'TVA 0% (achat)'}]
    #{'id': 17, 'name': 'TVA déductible (achat) 20,0%'}, 
    #{'id': 19, 'name': 'TVA déductible (achat) 10,0%'}, 
    #{'id': 20, 'name': 'TVA déductible (achat) 5,5%'}, 
    #{'id': 21, 'name': 'TVA déductible (achat) 2,1%'}, 
    return {
        '0.000' : 45,
        '2.100' : 21,
        '5.500' : 20,
        '10.000' : 19,
        '20.000' : 17,
    }

def _apiExecute(model,action='search_read',arg1={},arg2={}):
    global db,password,savedCurrentUid
    if savedCurrentUid <= 0:
        savedCurrentUid = _getConnexionUid()
    #Création du client
    #ATTENTION à l'URL /object et non /common :-)
    caisse = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(urlCaisse))
    out=caisse.execute_kw(db, savedCurrentUid, password, model, action,arg1,arg2)
    return out

#------------ Function  
def _getConnexionUid():
    global db,username,password
    #gaierror(-2, 'Name or service not known') => URL non correcte (en .com et non .fr par exemple)
    caisse = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(urlCaisse))
    uid = caisse.authenticate(db, username, password, {})
    if uid == False:
        raise ValueError("Echec de l'authentification")
    return uid
    
#Craétion du dict de lien entre l'id de catégorye Dolibar et l'ID Caisse
# Ce lien a créé lors de l'import du fichier category_child.csv dans l'interface de la caisse
def getCategoryExternalRef():
    out=_apiExecute('ir.model.data', 'search_read',[[['module','=','__import__'],['model','=','pos.category']]],{'fields': ['name','res_id']})#display_name
    categoryArray = {}
    for c in out:
        categoryArray[c['name']] = c['res_id']
    return categoryArray

def getProduct(isSell=None):
    #retourne les produits actif
    if isSell != None:
        search = [[['sale_ok' ,'=',isSell]]]
    else:
        search= {}
    return _apiExecute('product.template', 'search_read',search)

def getProductRef(isSell=None):
    #retourne les produits actif
    if isSell != None:
        search = [[['sale_ok' ,'=',isSell]]]
    else:
        search= {}
    return _apiExecute('product.template', 'search_read',search,{'fields': ['default_code']})

def getProductById(id):
    return _apiExecute('product.template', 'search_read',[[['id' ,'=',id]]])

def getProductByRef(ref):
    return _apiExecute('product.template', 'search_read',[[['default_code' ,'=',ref]]])

def getProductIdByRef(ref):
    return _apiExecute('product.template', 'search',[[['default_code' ,'=',ref]]])

#recherche une trné dans le journal des comptes
#utilsie un cache pour réduire les communications serveurs
def getLedgerByCode(code):
    try :
        global globalCacheHashLedger

        if code not in globalCacheHashLedger : 
            globalCacheHashLedger[code] = _apiExecute('account.account', 'search_read',[[['code' ,'=',code]]])[0]

        return globalCacheHashLedger[code]
    except:
        return []

def addLedger(code: str,name:str):
    productData = {
        'name': name,
        #'currency_id': False,
        'code': code,
        #'deprecated': False,
        'user_type_id': 14,#[14,'Income'],
        'internal_type': 'other',
        'internal_group': 'income',
        'last_time_entries_checked': False,
        'reconcile': False,
        #'tax_ids': [],
        #'note': False,
        #'company_id': [1, 'Ti Coop'],
        #'tag_ids': [],
        #'group_id': False,
        'display_name': code + ' ' + name,
        }
    
    _apiExecute('account.account', 'create', [productData])
    return True

#recherche d'un journal, si ne le trouve pas l'ajout
#puis retour l'id du produit
def getAddIdLedgerByCode(code: str, name:str):
    #recherche le code
    out = getLedgerByCode(code)
    if 'id' in out : 
        #le produit exsite, retour de l'id
        return out['id']
    else:
        #si ne trouve pas le champs id (et donc le journal), le crée
        addLedger(code,name)
        return getLedgerByCode(code)['id']

def getAccountingRecord(dateFilter:datetime,select=None, detail = False):
    #return _apiExecute('account.move', 'search_read',{})

    if select ==None:
        fields = {}
    else:
        fields = {"fields": select}

    if detail == False:
        base = 'account.move'
    else:
        base = 'account.move.line'

    return _apiExecute(base, 'search_read',
                            [[['create_date','>=',dateFilter.strftime("%Y-%m-%d %H:%M:%S")]]],
                            fields)
    
#Update or create product
#productData['default_code'] doit exsiter
def updateProduct(productData):
    try :
        #logger.info("     Recherche de l'article")
        #product.template et non product.product afin de gérer les prduits déja exsitant mais non vendu
        idProduit = getProductIdByRef(productData['default_code'])
        if len(idProduit) > 1:
            raise ValueError("Doublon dans la base de la caisse")
        else:
            if len(idProduit) == 1:
                #logger.info("     Mise à jour de l'article, id : "+str(idProduit[0]))
                #Vous ne pouvez pas modifier l'unité de mesure car il y a déjà des mouvements de stock pour cet article.
                #suppression de l'unité de musres si exite
                if 'uom_id'    in productData : del productData['uom_id']
                if 'uom_po_id' in productData : del productData['uom_po_id']
                if 'uom_name'  in productData : del productData['uom_name']
                _apiExecute('product.product', 'write', [idProduit[0], productData])

            else:
                #logger.info("     Création de l'article")
                _apiExecute('product.product', 'create', [productData])
        return True
    except Exception as error:
        raise Exception("updateProduct : "+ productData['default_code'] + "=> " + repr(error))

#archive le produit
def disableProduct(ref):
    #récupération du produit pour désactivatio
    productId = getProductIdByRef(ref)
    if len(productId) <= 0 : raise Exception("disableProduct : product not found " + str(ref))

    product = {} 
    product['active']           = 1#toujours 1, sinon supprime le default_code :-(
    product['sale_ok']          = 0
    product['purchase_ok']      = 0#pas de commande depuis la caisse
    product['available_in_pos'] = 0
    product['barcode']          = False
    return _apiExecute('product.product', 'write', [productId[0], product])

def exportRaw(table):
    return _apiExecute(table, 'search_read',{},{})

#Retour le statut comptable des session de vente de la péridoe données
#dateFilter = '2020-03-'
def getPOSSaleStatement(dateFilter:datetime):
    #pour debug, eport des donnes de la caisse
    #out=caisse.execute_kw(db, uid, password,  'pos.session', 'search_read',{},{})
    #out=caisse.execute_kw(db, uid, password,  'account.bank.statement', 'search_read',{},{})
    #out=caisse.execute_kw(db, uid, password,  'res.partner', 'search_read',{},{'fields': ['product_id','product_uom_id','qty_done','date']})#'display_name'
    #Récupération des sessions de la journée
    #dateFilter = dateFilter.strftime("%Y-%m-%d")
    sessionList=_apiExecute('pos.session', 'search_read',[[['stop_at','>=',dateFilter.strftime("%Y-%m-%d %H:%M:%S")],
                                                        ['state','=','closed']]],
                                                        {'fields': ['statement_ids','name','stop_at']})# 'order_ids', 'statement_ids', 'journal_ids': [11, 12, 10],
    #"Pour chauque session, recupration des etats de caisse"
    out= []
    for c in sessionList:
        statementList=_apiExecute('account.bank.statement', 'search_read',[[['id','in',c['statement_ids'] ]]],
                                                                           {'fields': ['journal_id','total_entry_encoding']})#,'name','date_done'
        #logger.info("Session terminee le : " + c['stop_at'] )
        for s in statementList:
            #print(s['journal_id']) #affiche le type de paiement
            out.append({"journal" : s['journal_id'][1],
                        "total" : s['total_entry_encoding'],
                        "date" : c['stop_at'],
                        "name" : c['name'],
            })
            
    
    return out

#Retourne l'état du stock
#searchUpdateDate.strftime("%Y-%m-%d")
def getStockStatut(dateFilter,dispacheByDate = False):

    #On récupère la liste des mouvements de stocks qui correspondent à l'inventaire (id=618)
    #On prépare la structure
    stockInventory=[]
    #On récupère les ids de mouvements depuis les commandes de Mme Inventaire (id=618)
    orderInventory=_apiExecute('pos.order', 'search_read', [[['partner_id', '=', [618]], ['date_order','>=',dateFilter.strftime("%Y-%m-%d %H:%M:%S")]]],{'fields' : ['picking_id']})

    #Pour chaque commandes passés
    for order in orderInventory:
        pickingId = order['picking_id'][0]
        #On récupère l'id du mouvement de stock associé
        stockInventory.append(_apiExecute('stock.move.line', 'search_read', [[['picking_id', '=', [pickingId]]]],{'fields' : ['id']})[0]['id'])

    #Calcul du stock de la journee 
    out=_apiExecute('stock.move.line', 'search_read',[[['state','=','done'],['date','>=',dateFilter.strftime("%Y-%m-%d %H:%M:%S")],['reference','like','POS%']]],
                                                    {'fields': ['id', 'product_id','qty_done','date']})#'product_uom_id'
    #Cette liste contient une entrée de chaque produit de chaque vente. 
    #L'objectif est de faire la somme des vente pour chaque produits.
    #ex: {'id': 91, 'product_id': [733, '[ALC-04] GRUNGE 8° (33 cl)'], 'product_uom_id': [1, 'Unit(s)'], 'qty_done': 3.0, 'date': '2020-03-14 14:30:32'}
    stockDay = {}
    for c in out:
        try:
            key = c['product_id'][1]
            qty = c['qty_done']
            #date = 2020-03-02 18:23:47 => récupération que de la date
            date = c['date'].split(" ")[0]
            #Récupération du ref ID dans le nom de l'aricle (evite une requete de plus avec l'id :-)
            #'[ALC-04] GRUNGE 8° (33 cl)' => '[ALC-04' => 'ALC-04'
            key = key.split("]")[0][1:]
            #si dispacheByDate est vrais, création d'un sous tableau avec une entrée par date
            if dispacheByDate == True:
                #création d'une entrée vide, si elle n'exsite pas
                if key not in stockDay:
                    stockDay[key] = {}
                #création d'une entrée de date vide, si elle n'existe pas
                if date not in stockDay[key]:
                    stockDay[key][date] = 0

                #Si c'est un mouvement qui appartient à l'inventaire, on SOUSTRAIT la quantité
                if c['id'] in stockInventory:
                    stockDay[key][date] -= qty
                else:
                    stockDay[key][date] += qty
            else:  
                #création d'une entrée vide, si elle n'exsite pas
                if key not in stockDay:
                    stockDay[key] = 0
                #mise à jour du stock, si c'est un mouvement qui appartient à l'inventaire, on SOUSTRAIT la quantité
                if c['id'] in stockInventory:
                    stockDay[key][date] -= qty
                else:
                    stockDay[key][date] += qty
        except Exception:
           raise Exception('WARNING, Reference du produit non conforme -> ' + c)
    return stockDay

def test():
    return _apiExecute('pos.category', 'search_read', [],{'limit': 1})

def getPartner():
    return _apiExecute('res.partner', 'search_read',{},{})

#Import un fichier pour alimenter les coopeateurs dans la caisse
#["External ID","name","email","street","city","zip","active",])
#retourn le nombre de cooperateur importé, sinon raise une exception

#les API ne sont pas utilisable (besoin du droit admin ?), utilisation de selenium pour faire un import
#out=caisse.execute_kw(db, uid, password,  'res.partner', 'search_read',{},{})
#pip3 install selenium
#Fonction à executer depuis un container docker fedora
def importCooperator(importFile):
    global urlCaisse, db,username,password

    dir_path = os.path.dirname(os.path.realpath(__file__))
    firefoxDriver = dir_path+"/geckodriver"

    #import de selenium pour piloter le navigateur
    from selenium.webdriver import Firefox
    from selenium.webdriver.firefox.options import Options
    from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC

    options = Options()
    options.headless = True
    #profile = FirefoxProfile()
    #profile.set_preference("webdriver.log.file", "/tmp/TiCoopScript-SeleniumFirefoxConsole.log")
    #browser = Firefox(executable_path=firefoxDriver, options=options,firefox_profile=profile)
    debug("Creation de l'objet selenimum")
    browser = Firefox(executable_path=firefoxDriver, options=options,log_path='/tmp/TiCoopScript-SeleniumFirefoxConsole.log')
    
    try:
        try:
            #connexion a l'envrionement
            debug("connexion a l'envrionement : " + urlCaisse+'/web?db='+db)
            browser.get(urlCaisse+'/web?db='+db)
            debug("Attente ouverture de la page")
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.ID, "login")))
            #saisie du login
            debug("saisie du login")
            browser.find_element_by_id("login").send_keys(username)
            browser.find_element_by_id("password").send_keys(password + Keys.RETURN)
            #attente de la fin de l'authentification
            debug("attente de la fin de l'authentification")
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "o_web_client")))
        except Exception as error:
            raise Exception("connexion a l'envrionement : " + repr(error))

        try:
            #Connexion au portail d'upload
            debug("ouverture de la page d'import")
            url = urlCaisse+'/web#model=res.partner&action=import&menu_id=175'
            debug(url)
            browser.get(url)
            debug("attente de l'ouveture de la page")
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "oe_import_file")))
        except Exception as error:
            raise Exception("ouverture de la page d'import :"+repr(error))

        try:
            debug("Selection et upload du fichier : " + importFile)
            browser.find_element_by_xpath("//input[@class='oe_import_file']").send_keys(importFile)
            #pas besoin de valider le formulaire, c'est autonomatique
            #browser.find_element_by_xpath("//form[@class='oe_import']").submit()
            debug("Attente de l'ouverture de la page de traitement du fichier")
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "oe_import_fields")))
        except Exception as error:
            raise Exception("upload du fchier : "+repr(error))
        
        try:
            debug("Lancement de l'import")
            browser.find_element_by_xpath("//div[@class='o_cp_buttons']/button[contains(text(),'Importer')]").click()
            #recuperation du resultat
            debug("Attente de la fenetre de retour de l'état de l'import")
            WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "o_notification_content")))
        except Exception as error:
            raise Exception("import du fichier : "+repr(error))

        try :
            #html = browser.find_element_by_xpath("//div[@class='o_notification_manager']").get_attribute('innerHTML')
            #<div role="alertdialog" class="o_notification " style="opacity: 0.0177213;">
            #    <div class="o_notification_title">
            #        <span role="img" aria-label="Notification undefined" class="o_icon fa fa-3x fa-lightbulb-o" title="Notification undefined"></span>
            #        Import terminé
            #    </div>
            #    <div class="o_notification_content">4 enregistrements ont été importés avec succ</div>
            #</div>
            debug("Recuperation du contenue de la fenetre de notification")
            msg = str(browser.find_element_by_xpath("//div[@class='o_notification_content']").get_attribute('innerHTML'))
            debug("Contenue :" + msg)
            #pas besoin de tester le retour, si la chaine n'est pas trouvee, une erreur est levee
            debug("Test si c'est un success")
            msg.index("succ")
            debug("C'est un success, recupeation de nombre de ligne")
            #récupération du 1er nombre qui indique le nombre d'import réussi
            nbImported = msg.split(" ")[0]
            debug("Nombre de ligne : " + nbImported)
            #WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CLASS_NAME, "toto")))
            browser.quit()
            return int(nbImported)
        except Exception as error:
            raise Exception("resultat de l'import: " + msg + "/" + repr(error))

    except Exception as error:
        raise Exception("caisse importCooperator : " + repr(error))
    finally:
        browser.quit()

def debug(msg):
    #print (msg)
    return


def getAccountTax(select=None):

    if select ==None:
        fields = {}
    else:
        fields = {"fields": select}

    return _apiExecute('account.tax', 'search_read',{},fields)