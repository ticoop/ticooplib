from datetime import datetime
from typing import List

class ErpApi():

    def __init__(
        self,
        base_url:str,
        api_token:str,
        username:str,
        password:str,
        default_get_limit:int,
        default_sort_order:str,
        default_begin_date:datetime,
        default_end_date:datetime,
    ):

        raise NotImplementedError()

    def test_connexion(
        self
    ) -> bool:

        raise NotImplementedError()


    def get_product_and_or_service(
        self,
        mode:int        = 0,
        is_sell:bool    = True,
        get_limit:int   = None,
        sort_order:str  = None,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()

    def get_product(
        self,
        is_sell:bool    = True,
        get_limit:int   = None,
        sort_order:str  = None,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_product_by_id(
        self,
        product_id:int,
    ) -> dict:
    
        raise NotImplementedError()


    def get_product_by_ref(
        self,
        product_ref:str,
    ) -> List[dict]:
    

        raise NotImplementedError()


    def get_product_by_barcode(
        self,
        product_barcode:str,
    ) -> List[dict]:
    
        raise NotImplementedError()


    def get_product_and_service(
        self,
        is_sell:bool    = True,
        get_limit:int   = None,
        sort_order:str  = None,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
        
        raise NotImplementedError()


    def get_product_and_service_by_ref(
        self,
        product_ref:str,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_id_new_product(self, filter):
        raise NotImplementedError()



    def update_product(
        self,
        product_id:int,
        product_updated_data:dict,
    ) -> bool:
     
        raise NotImplementedError()


    def delete_product(
        self,
        product_id:int,
    ) -> bool:
        
        raise NotImplementedError()
        

    def get_product_picture(
        self,
        product_id:int,
    ) -> str:

        raise NotImplementedError()



    def get_category_external_ref(
        self,
    ) -> dict:
        
        raise NotImplementedError()





    def get_cooperator(
        self,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def import_cooperator(
        self,
        import_file:str,
    ) -> int:
    
        raise NotImplementedError()
    

    def get_invoices(
        self,
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> List[dict]:
    
        raise NotImplementedError()


    def get_supplier_invoices(
        self,
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> List[dict]:
    
        raise NotImplementedError()




    def get_supplier(
        self,
        put_in_array:bool = False,
    ) -> List[dict]:
    
        raise NotImplementedError()


    def get_supplier_by_id(
        self,
        supplier_id:int,
    ) -> dict:
        
        raise NotImplementedError()


    def get_supplier_name_by_product_id(
        self,
        product_id:int,
    ) -> str:
     
        raise NotImplementedError()



    def get_stock_statut(
        self,
        dispatch_by_date:bool = False,
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> dict:
     
        raise NotImplementedError()


    def get_stock_movement(
        self,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
    
        raise NotImplementedError()


    def get_date_of_last_stock_movement(
        self,
    ) -> datetime:

        raise NotImplementedError()


    def set_stock_movement_by_product_id(
        self,
        product_id:int,
        qty:int,
        movement_label:str,
    ) -> int:
    
        raise NotImplementedError()



    def get_account_by_code(
        self,
        account_code:int,
    ) -> dict:

        raise NotImplementedError()


    def get_account_record(
        self,
        fields:dict = None,
        detail:bool = False,        
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def add_account(
        self,
        acound_code:int,
        account_name:str,
    ) -> int:

        raise NotImplementedError()


    def get_bank_account_line(
        self,
        account_id:int = 1,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_date_of_last_bank_account_auto_entry(
        self,
        account_id:int = 1,
    ) -> datetime:
     
        raise NotImplementedError()

    # def add_bank_account_auto_entry(
    #   self,
    #   type:PaymentType,
    #   label:str,
    #   date:datetime,
    #   amount:float,
    #   account_id:int = 1
    # ) -> int:
        
    #    raise NotImplementedError()


    def get_account_tax(
        self,
        fields:dict = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_pos_sale_statement(
        self,
        stop_after_date:datetime  = None,
        stop_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    # def withdraw_option(
    #     self,
    #     obj_product,
    #     option_name
    # ):
    
    #     raise NotImplementedError()

    # def withdrawPrixKG(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawPrixL(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawWeightUnits(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawVolumeUnits(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesure(self, objProduit):
    #      raise NotImplementedError()

    # def withdrawMainMesure(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesureUnite(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesurePrice(self, objProduit):
    #     raise NotImplementedError()





    # def floatToString(self, value):
    #     raise NotImplementedError()

    # def isFleg(self, productArray):
    #     raise NotImplementedError()

    # def getDicTVA(self):
    #     raise NotImplementedError()

    # def getDicSupplierTVA(self):
    #     raise NotImplementedError()