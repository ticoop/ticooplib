#!/usr/bin/python
#
#Version 1.0 - 2020-03-25 - Cyril CARGOU - Script d'export des produits et création des étiquettes
#
#python3 ./script.py


#import du service client REST API
import requests
from requests.auth import HTTPBasicAuth
import json
#pour la gestion des accents
import unicodedata
#pour la gestion des regex
import re
#pour le filtre des articles modifiés
import datetime,time


#------------- Variables ------------------
#Token de connexion à Dolibarr avec le compte de service export Data
#Pour générer le jeton (ou depuis l'interface web du compte utilisateur): https://ERP.BASE.URL/api/index.php/login?login=ser&password=TODO[&reset=1]
#https://ERP.BASE.URL/api/index.php/explorer
apiKey = 'TO_BE_SET'
baseURL = 'TO_BE_SET'
inventorycode="AutoUpdate01"
bankAutoAddcode="AutoAdd"

#Pour gagner du temps pour la récupartion des non des fournisseur associée à un produit
cacheSupplier = None

# La limite par défaut est 100 ce qui est souvent trop petit pour récupérer l'ensemble des résultats. Une limite à -1 est l'infini
limitALL = 'limit=-1'

#Fonction prive de requetes API
def _apiGet(URI):
    return _api(URI, 'GET')

#Fonction prive de requetes API
def _apiPost(URI,data):
    return _api(URI, 'POST',data)

#Fonction prive de requetes API
def _apiPut(URI,data):
    return _api(URI, 'PUT',json.dumps(data))

#Fonction prive de requetes API
def _api(URI,type='GET',data=None):
    global baseURL, apiKey
    headers = {'DOLAPIKEY': apiKey}
    url = baseURL  + URI

    if  type == 'GET':
        response = requests.get(url, headers=headers)
    elif type == 'POST':
        response = requests.post(url, headers=headers, data = data)
    elif type == 'PUT':
        headers['Accept'] = 'application/json'
        headers['Content-Type'] = 'application/json'
        response = requests.put(url, headers=headers, data = data)
    else:
        raise Exception('_api request type unknow : ' + type)
    #print(response)
    #print(response.headers)
    #print(response.text)
    #print(response.content)
    #print(response.encoding)
    #response.raise_for_status()
    if response.status_code == 404:#pas de data
        return []

    if response.status_code != 200:
        raise Exception('_api status_code : ' + str(response.status_code) + " URL : " + url)

    return response.json()

#test si la connexion est OK
def testConnexion():
    try:
        response = _apiGet('/api/index.php/status')
        if response['success']['code'] != 200:
            return False
        return True
    except Exception:
        return False

#Récupération de la liste des produits à partire d'un date
#isSell : True : récupration de la liste des poduit actif, False: produit inctif, None : All
#date de modification des produits avant datatime
#date de modification des produits  après datatime
# limit : nombre max de produits à récupérer
def getProduct (isSell = None,modifiedAfterDate=datetime.datetime(1970,1,1,0,0,0),modifiedBeforDate=datetime.datetime(2970,1,1,0,0,0),limit='3000'):
    #creation du filtre
    filtre = "&sqlfilters=(t.tms>='"+modifiedAfterDate.strftime("%Y-%m-%d %H:%M:%S")+"')AND(t.tms<='"+modifiedBeforDate.strftime("%Y-%m-%d %H:%M:%S")+"')"
    if isSell != None: 
        bs = '1' 
        if isSell == False: bs = '0'
        filtre += "AND(t.tosell='"+bs+"')"
    
    url = '/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit='+limit+'&mode=1'+filtre
    response = _apiGet(url)
    nbArticle = len(response)
    if nbArticle < 1:
        raise Exception('Nombre d\'article inférieur à 1  : ' + str(nbArticle))
    return response

def getProductById(id):
    # On attend un dict (c'est ce que renvoi cette url si tout se passe bien)
    url = "/api/index.php/products/" + str(id)
    response = _apiGet(url)

    # _apiGet() appelle _api() qui renvoi une liste si le code retour est 404
    if isinstance(response, list):
        raise Exception("Produit non trouvé")
        
    return response

def getProductByBarcode (barcode, limit='3000'):
    filtre = "&sqlfilters=(t.barcode LIKE '" + barcode + "')"
    url = '/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit='+limit+'&mode=1'+filtre

    return _apiGet(url)
        
    
#Récupération de la liste des produits et des services à partire d'un date
#isSell : True : récupration de la liste des poduit actif, False: produit inctif, None : All
#date de modification des produits avant datatime
#date de modification des produits  après datatime
def getProductAndService (isSell = None,modifiedAfterDate=datetime.datetime(1970,1,1,0,0,0),modifiedBeforDate=datetime.datetime(2970,1,1,0,0,0) ):
    #creation du filtre
    filtre = "&sqlfilters=(t.tms>='"+modifiedAfterDate.strftime("%Y-%m-%d %H:%M:%S")+"')AND(t.tms<='"+modifiedBeforDate.strftime("%Y-%m-%d %H:%M:%S")+"')"
    if isSell != None: 
        bs = '1' 
        if isSell == False: bs = '0'
        filtre += "AND(t.tosell='"+bs+"')"
    
    url = '/api/index.php/products?sortfield=t.ref&sortorder=ASC&'+limitALL+'&mode=0'+filtre
    response = _apiGet(url)
    nbArticle = len(response)
    if nbArticle < 1:
        raise Exception('Nombre d\'article inférieur à 1  : ' + str(nbArticle))
    return response

#Fonction qui crée un mouvement de stocke d'un produit
def updateProductById(id,data):
    url = "/api/index.php/products/"+str(id)
    return _apiPut(url,data)

#Fonction qui crée un mouvement de stocke d'un produit
def updateProductByRef(ref,data):
    return updateProductById(getProductIdByRef(ref),data)

#Pour chaque article récupération de l'image (si existe)
#retour l'image en base64
def getProdcutPicture (productId):
    try:
        url = '/api/index.php/documents?modulepart=product&id='+productId
        #ne selection qu'un fichier qui est une image
        files = _apiGet(url)
        selectedFile = None
        for f in files:
            if f['name'].lower().endswith(('.png', '.jpg', '.jpeg', '.gif')):
                selectedFile = f #si pas d'image lance une exception
                break
        #récupération de l'image
        if selectedFile == None : return ""
        url = '/api/index.php/documents/download?module_part=product&original_file='+selectedFile['level1name']+'%2F'+selectedFile['relativename']
        return _apiGet(url)['content']
    except Exception:
        return ""

#Export des cooperateurs
def getCooperator(modifiedAfterDate = None):
    filtre = ""
    if modifiedAfterDate != None:
        filtre = "&sqlfilters=(t.tms>='"+modifiedAfterDate.strftime("%Y-%m-%d %H:%M:%S")+"')"
    url = '/api/index.php/members?sortfield=t.rowid&sortorder=ASC&limit=2000'+filtre
    return _apiGet(url)

def getActiveCooperator():
    filtre = "&sqlfilters=(t.statut='1')"
    url = '/api/index.php/members?sortfield=t.rowid&sortorder=ASC&limit=2000'+filtre
    return _apiGet(url)

#Fonction qui retourne le produit ERP à partire de sa reférence
def getProductByRef(refId):
    url = "/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit=1&mode=1&sqlfilters=(t.ref:=:'"+refId+"')"
    response = _apiGet(url)
    nb = len(response)
    if nb != 1:
        raise Exception('Article (ref : '+refId+') non trouve ou non unique : ' + str(nb))
    return response[0]

#Fonction qui retourne le produit ERP à partire de sa reférence
def getProductOrServiceByRef(refId):
    url = "/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit=1&mode=0&sqlfilters=(t.ref:=:'"+refId+"')"
    response = _apiGet(url)
    nb = len(response)
    if nb != 1:
        raise Exception('Article (ref : '+refId+') non trouve ou non unique : ' + str(nb))
    return response[0]

#Fonction qui retourne l'ID d'un produit ERP à partire de sa reférance
def getProductIdByRef(refId):
    return getProductByRef(refId)['id']

def getProductSellAccountByRef(refId):
    return getProductByRef(refId)['accountancy_code_sell']

def getInvoices():
    url = "/api/index.php/invoices?sortfield=t.rowid&sortorder=ASC&limit=2000"
    response = _apiGet(url)
    return response

def getSupplierInvoices(dateDebut, dateFin):
    filtre = "&sqlfilters=(t.datec>='"+dateDebut.strftime("%Y-%m-%d %H:%M:%S")+"')and(t.datec<'"+dateFin.strftime("%Y-%m-%d %H:%M:%S")+"')"
    url = "/api/index.php/supplierinvoices?sortfield=t.rowid&sortorder=ASC&limit=2000"+filtre
    response = _apiGet(url)
    return response

def getSupplier(putInArray=False):
    #récupération du fourniseur de ce stock
    url = "/api/index.php/thirdparties?sortfield=t.rowid&sortorder=ASC&limit=2000"
    response = _apiGet(url)
    #pas de mise en page
    if putInArray== False  :
        return response
    #classement afin de faciliter l'identification du fourniseur par ID
    out = {}
    for s in response:
        out[s['id']] = s 
    return out

def getSupplierById(id):
    url = "/api/index.php/thirdparties/" + str(id)
    return _apiGet(url)

def getProductSupplierName(idProduct):
    global cacheSupplier
    try :
        #récupération du derniers mouvement de stock liée à ce produit
        url = "/api/index.php/stockmovements?sortfield=t.tms&sortorder=DESC&limit=1&sqlfilters=(t.fk_product%3D"+str(idProduct)+")"
        response = _apiGet(url)[0]

        #création du cache
        if cacheSupplier == None:
            cacheSupplier = getSupplier(putInArray=True)

        return cacheSupplier[response['fk_user_author']]['name']
    except Exception:
        return "None"

#Fonction qui return la derniere date de mise à jour du stock
def getStockMovement(modifiedAfterDate = None):
    try:
        global inventorycode

        filtre = ""
        if modifiedAfterDate != None:
            filtre = "and(t.tms>='"+modifiedAfterDate.strftime("%Y-%m-%d %H:%M:%S")+"')"

        url = "/api/index.php/stockmovements?sortfield=t.tms&sortorder=DESC&limit=20000&sqlfilters=(t.inventorycode:=:'"+inventorycode+"')"+filtre
        return _apiGet(url)

    except Exception:
        return None

#Fonction qui return la derniere date de mise à jour du stock
def getStockMovementLastEntryDate():
    try:

        global inventorycode
        #Utilisation de la date du dernier entrgistrment => refusé car ne non suffsisaement présci
        #global logger, inventorycode
        url = "/api/index.php/stockmovements?sortfield=t.tms&sortorder=DESC&limit=1&sqlfilters=(t.inventorycode:=:'"+inventorycode+"')"
        response = _apiGet(url)
        nb = len(response)
        if nb != 1:
            return None
        return datetime.datetime.fromtimestamp(response[0]['tms'])

        #Utilisation du labelle et récupération de la date, non utilsé car non necessaire.
        #global logger, inventorycode
        #url = "/api/index.php/stockmovements?sortfield=t.label&sortorder=DESC&limit=1&sqlfilters=(t.inventorycode%3A%3D%3A'"+inventorycode+"')"
        #url = "/api/index.php/stockmovements?sortfield=t.label&sortorder=DESC&limit=1&sqlfilters=(t.inventorycode:=:'"+inventorycode+"')"
        #response = _apiGet(url)
        #nb = len(response)
        #if nb != 1:
        #    return None
        #label = response[0]['label']
        #extraction de la date, recherche une date au format : '2020-03-14'
        #x = re.search("([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})", label)
        #dateStr = x.group()
        #return datetime.datetime.strptime(dateStr, "%Y-%m-%d %H:%M:%S")
    
    except Exception:
        return None

#Fonction qui crée un mouvement de stocke d'un produit
def setStockMovementByProductId(id,stockChange, info):
    global  inventorycode
    url = "/api/index.php/stockmovements"
    data = {'product_id': id, 
        'warehouse_id':1, #entrepot par défaut
        'qty':stockChange, 
        'movementcode':inventorycode,
        'movementlabel':info
        }
   
    response = _apiPost(url,data)
    idMouvement = int(response)
    if idMouvement <= 0:
        raise Exception('setStockMovementByProductId : Mouvement en erreur : ' + response)
    return idMouvement

def getBankaccountsLine(modifiedAfterDate = None,accountId = "1"):
    global bankAutoAddcode
    filtre = ""
    if modifiedAfterDate != None:
        filtre = "and(t.datec>='"+modifiedAfterDate.strftime("%Y-%m-%d %H:%M:%S")+"')"
    url = "/api/index.php/bankaccounts/"+accountId+"/lines?sqlfilters=(t.label:like:'*"+bankAutoAddcode+"*')"+filtre

    result = _apiGet(url)
    #LA version 11 API Dolibarr ne support par de filtre pour cette requete
    #de fait, filtre manuelle
    out = []
    for l in result:
        if bankAutoAddcode not in l['label']: continue
        if modifiedAfterDate != None: 
            if datetime.datetime.strptime(l['datec'], "%Y-%m-%d %H:%M:%S") < modifiedAfterDate : continue
        out.append(l)

    return out

def getBankaccountsLastAutoEntryDate (accountId = "1"):
    global bankAutoAddcode
    #recherche les entrées autonomatiques
    lines = getBankaccountsLine(accountId)
    #defintion d'une date ancinne qui servira de challange pour la 1ere date 
    lastDate = datetime.datetime.strptime("1970-01-01", "%Y-%m-%d")
   
    #récupation de la date de fermeture de caisse inscrite dans le label par la fonction  addBankaccountsAutoEntry
    # #label = XXXX -  bankAutoAddcode YYYY-mm-dd
    find = False
    for l in lines:
        if bankAutoAddcode not in l['label']: continue
        #récupération du la date  au format : '2020-03-14 hh:mm:ss'
        dateStr = re.search("([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})",  l['label']).group()
        if dateStr == None: continue
        
        #converti en date
        currentDate = datetime.datetime.strptime(dateStr, "%Y-%m-%d %H:%M:%S")
       
        if lastDate < currentDate:
            #une date plus recente est trouvée
            lastDate = currentDate
            find = True

    if find == False:
        lastDate = None

    return  lastDate


class PaymentType():
    Cartebancaire = "CB"
    Cheque = "CHQ"
    Espece = "LIQ"
    OrdrePrelevement = "PRE"
    VirementBancaire = "VIR"

def addBankaccountsAutoEntry(type : PaymentType, label : str, date : datetime, amount : float, accountId = "1" ):
    global bankAutoAddcode
    url = "/api/index.php/bankaccounts/"+accountId+"/lines"
    data = {
        "date": str(int(time.time())),#int pour ne pas avoir la virgule du timestamp
        "type": type,
        "label": label + " - "+bankAutoAddcode+" "+ date.strftime("%Y-%m-%d %H:%M:%S"),
        "amount": amount
    }
    response = _apiPost(url,data)
    idMouvement = int(response)
    if idMouvement <= 0:
        raise Exception('setBankaccountsLine : Creation de la ligne en erreur : ' + response)
    return idMouvement

#extrction de l'object l'option demandée
def withdrawOption(objProduit, optionName):
    optionName = 'options_' + optionName
    if 'array_options' in objProduit and optionName in objProduit['array_options'] : 
        return objProduit['array_options'][optionName]
    return None

#function qui calcul le prix aux KG à partire d'un produit
def withdrawPrixKG(objProduit):
    if objProduit['weight'] == None: 
        return None
    else:
        unite = 1 #2 = Kg, par défaut
        if objProduit['weight_units'] == "3" :
            unite = 1000  #3= g
        #poid en gramme ?
        return round(float(objProduit['price_ttc']) / float(objProduit['weight']) * unite,2)

#function qui calcul le prix aux L à partire d'un produit
def withdrawPrixL(objProduit):
    if objProduit['volume'] == None: 
        return None
    else:
        unite = 1 #20 = L, par défaut
        if objProduit['volume_units'] == "21" :  #21= ml
            unite = 1000 
        #poid en gramme ?
        return round(float(objProduit['price_ttc']) / float(objProduit['volume']) * unite,2)

def withdrawWeightUnits(objProduit):
    if objProduit['weight'] == None : return None #valeur non définie
    if objProduit['weight_units'] == "1" : return "Tonne"
    if objProduit['weight_units'] == "2" : return "Kg"
    if objProduit['weight_units'] == "3" : return "g"
    if objProduit['weight_units'] == "4" : return "mg"
    if objProduit['weight_units'] == "5" : return "once"
    if objProduit['weight_units'] == "6" : return "livre"
    return None

def withdrawVolumeUnits(objProduit):
    if objProduit['volume'] == None : return None #valeur non définie
    if objProduit['volume_units'] == "19" : return "m³"
    if objProduit['volume_units'] == "20" : return "L"
    if objProduit['volume_units'] == "21" : return "ml"
    if objProduit['volume_units'] == "22" : return "µl"
    if objProduit['volume_units'] == "23" : return "pied³"
    if objProduit['volume_units'] == "24" : return "pouce³"
    if objProduit['volume_units'] == "25" : return "once"
    if objProduit['volume_units'] == "27" : return "gallon"
    return None

#Fonction qui retour la valeur Kg ou poid
def withdrawMesure(objProduit):
    if objProduit['weight'] != None : return objProduit['weight']
    if objProduit['volume'] != None : return objProduit['volume']
    return None

#Fonction qui retour la meusure Kg ou poid
def withdrawMainMesure(objProduit):
    if objProduit['weight'] != None : return "Kg"
    if objProduit['volume'] != None : return "L"
    return None
#Fonction qui retour l'unité de Kg ou L
def withdrawMesureUnite(objProduit):
    out = withdrawWeightUnits(objProduit)
    if out != None: return out
    return withdrawVolumeUnits(objProduit)
#Fonction qui retour le prix au Kg ou L
def withdrawMesurePrice(objProduit):
    out = withdrawPrixKG(objProduit)
    if out != None: return out
    return withdrawPrixL(objProduit)


#mise en forme des nombres
def floatToString(value):
    if value == None : return None
    return str(round(float(value),2)).replace(".",",")

#mise en forme des nombres
def isFleg(productArray):
    try:
        code = productArray['accountancy_code_sell']
        if code == "70710001" or code == "70710002":
            return True

        return False
    except Exception:
        return False
