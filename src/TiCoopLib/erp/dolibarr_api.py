from typing import List

from .erp_api import ErpApi

from datetime import datetime
import json
import requests
from urllib.error import HTTPError
# from requests.auth import HTTPBasicAuth

class DolibarrApi(ErpApi):

    def __init__(
        self,
        base_url:str,
        api_token:str,
        username:str,
        password:str,
        default_get_limit:int,
        default_sort_order:str,
        default_begin_date:datetime,
        default_end_date:datetime,
    ):
        
        if api_token is None:
            raise AttributeError("api_token must be defined")

        self.base_url   = base_url
        self.api_token  = api_token
        self.username   = None      # Not use
        self.password   = None      # Not use


        if default_get_limit is None:
            self.default_get_limit = -1
        else:
            self.default_get_limit  = default_get_limit        
        self.default_sort_order = default_sort_order
        self.default_begin_date = default_begin_date
        self.default_end_date   = default_end_date

        self.dolibarr_datetime_format = "%Y-%m-%d %H:%M:%S"
        self.dolibarr_uri = {}
        self.dolibarr_uri['base']               = "/api/index.php/"
        self.dolibarr_uri['status']             = "status/"
        self.dolibarr_uri['products']           = "products/"
        self.dolibarr_uri['documents']          = "documents/"
        self.dolibarr_uri['members']            = "members/"
        self.dolibarr_uri['invoices']           = "invoices/"
        self.dolibarr_uri['supplier_invoices']  = "supplierinvoices/"
        self.dolibarr_uri['suppliers']          = "thirdparties/"
        self.dolibarr_uri['stock_movements']    = "stockmovements/"


    def _api_get(
        self,
        uri:str,
    ): # -> List[dict] | dict
    
        return self._api(uri, http_method='GET')


    def _api_post(
        self,
        uri:str,
        data,
    ): # -> List[dict] | dict
    
        return self._api(uri, http_method='POST', data=data)


    def _api_put(
        self,
        uri:str,
        data,
    ): # -> List[dict] | dict
    
        return self._api(uri, http_method='PUT', data=json.dumps(data))


    def _api(
        self,
        uri:str,
        data = None,
        http_method:str = 'GET',
    ): # -> List[dict] | dict

        # DEFINE HEADERS AND URL
    
        headers = {'DOLAPIKEY': self.api_token}
        url = self.base_url  + self.dolibarr_uri['base'] + uri


        # CALL HTTP METHOD

        if  http_method == 'GET':
            response = requests.get(url, headers=headers)

        elif http_method == 'POST':
            response = requests.post(url, headers=headers, data=data)

        elif http_method == 'PUT':
            headers['Accept'] = 'application/json'
            headers['Content-Type'] = 'application/json'
            response = requests.put(url, headers=headers, data=data)

        else:
            raise HTTPError(
                url,
                code=405,
                msg='Method Not Allowed : ' + http_method,
                hdrs=headers,
                fp=None
            )


        # READ RESPONSE

        # !!! Lorsque Dolibarr ne trouve pas de data, il renvoi un 404
        if response.status_code == 404:  
            raise ValueError("No result found")

        if response.status_code != 200:
            raise HTTPError(
                url,
                code=response.status_code,
                msg=response.text + "\n" + url,
                hdrs=headers,
                fp=None
            )

        return response.json()


    def test_connexion(
        self
    ) -> bool:

        response = self._api_get(self.dolibarr_uri['status'])

        if response['success']['code'] != 200:
            return False

        return True


    def _get_common_param(
        self,
        sort_field:str  = "t.rowid",
        sort_order:str  = None,
        get_limit:int   = None,
    ) -> str:

        common_param = "?"

        if sort_field is None:
            sort_field = ''

        common_param += "sortfield=%s" % sort_field

        if sort_order is None:
            sort_order = self.default_sort_order

        common_param += "&sortorder=%s" % sort_order

        if get_limit is None:
            get_limit = self.default_get_limit

        common_param += "&limit=%d" % get_limit

        return common_param


    def _create_uri(
        self,
        module_uri:str,
        get_limit:int                    = None,
        sort_order:str                   = None,
        additionnals_param:dict          = None,
        dolibarr_created_date_field:str  = 'datec',
        created_after_date:datetime      = None,
        created_before_date:datetime     = None,
        dolibarr_modified_date_field:str = 'tms',
        modified_after_date:datetime     = None,
        modified_before_date:datetime    = None,
        additionnals_sql_filters:dict    = None,
    ) -> str:

        # CHECK OPTIONAL PARAMS

        if created_after_date is None:
            created_after_date = self.default_begin_date

        if created_before_date is None:
            created_before_date = self.default_end_date

        if modified_after_date is None:
            modified_after_date = self.default_begin_date

        if modified_before_date is None:
            modified_before_date = self.default_end_date

        # DEFINE SQL FILTERS

        sql_filter = {}
        sql_filter['created_after_date']    = "(t.%s>='%s')" % (dolibarr_created_date_field,  created_after_date.strftime(self.dolibarr_datetime_format))
        sql_filter['created_before_date']   = "(t.%s<='%s')" % (dolibarr_created_date_field,  created_before_date.strftime(self.dolibarr_datetime_format))
        sql_filter['modified_after_date']   = "(t.%s>='%s')" % (dolibarr_modified_date_field, modified_after_date.strftime(self.dolibarr_datetime_format))
        sql_filter['modified_before_date']  = "(t.%s<='%s')" % (dolibarr_modified_date_field, modified_before_date.strftime(self.dolibarr_datetime_format))    


        # DEFINE URI AND PARAMS
    
        uri = module_uri
        uri += self._get_common_param(sort_order=sort_order, get_limit=get_limit)

        if additionnals_param is not None:
            for param_name, param_value in additionnals_param.items():
                uri += "&%s=%s" % (param_name, param_value)

        uri += "&sqlfilters="
        for key, condition in sql_filter.items():
            uri += condition + "AND"

        if additionnals_sql_filters is not None:
            for key, condition in additionnals_sql_filters.items():
                uri += condition + "AND"

        uri = uri[:len(uri)-3] # Remove last AND

        return uri


    def get_product_and_or_service(
        self,
        mode:int                      = 0,
        is_sell:bool                  = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:


        if is_sell:
            is_sell_filter = "(t.tosell='1')"
        else:
            is_sell_filter = "(t.tosell='0')"

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['products'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            additionnals_param       = {'mode' : str(mode)},
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
            additionnals_sql_filters = {'is_sell' : is_sell_filter}
        )


        # GET RESPONSE

        response = self._api_get(uri)
        nb_product = len(response)

        if nb_product < 1:
            raise ValueError("Nombre de produit inférieur à 1 : %d" % nb_product)

        return response


    def get_product_and_or_service_by_ref(
        self,
        product_ref:str,
        mode:int                      = 0,
        is_sell:bool                  = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,      
    ) -> List[dict]:

        if is_sell:
            is_sell_filter = "(t.tosell='1')"
        else:
            is_sell_filter = "(t.tosell='0')"

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['products'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            additionnals_param       = {'mode' : str(mode)},
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
            additionnals_sql_filters = {
                'is_sell' : is_sell_filter,
                'product_ref' : "(t.ref LIKE '%s')" % product_ref,
            }
        )
        

        # GET RESPONSE

        response = self._api_get(uri)
        nb_product = len(response)

        if nb_product < 1:
            raise ValueError("Nombre de produit inférieur à 1 : %d" % nb_product)

        return response


    def get_product(
        self,
        is_sell:bool                  = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        return self.get_product_and_or_service(
            mode                    = 1, # Only products
            is_sell                 = is_sell,
            get_limit               = get_limit,
            sort_order              = sort_order,
            created_after_date      = created_after_date,
            created_before_date     = created_before_date,
            modified_after_date     = modified_after_date,
            modified_before_date    = modified_before_date,
        )


    def get_product_by_id(
        self,
        product_id:int,
    ) -> dict:
    
        uri = self.dolibarr_uri['product'] + str(product_id)
        response = self._api_get(uri)
            
        return response                                         


    def get_product_by_ref(
        self,
        product_ref:str,
        is_sell:bool                  = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
    

        return self.get_product_and_or_service_by_ref(
            product_ref             = product_ref,
            mode                    = 1, # Only products
            is_sell                 = is_sell,
            get_limit               = get_limit,
            sort_order              = sort_order,
            created_after_date      = created_after_date,
            created_before_date     = created_before_date,
            modified_after_date     = modified_after_date,
            modified_before_date    = modified_before_date,
        )


    def get_product_by_barcode(
        self,
        product_barcode:str,
        is_sell:bool                  = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,       
    ) -> List[dict]:
    

        if is_sell:
            is_sell_filter = "(t.tosell='1')"
        else:
            is_sell_filter = "(t.tosell='0')"

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['products'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            additionnals_param       = {'mode' : '1'},
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
            additionnals_sql_filters = {
                'is_sell' : is_sell_filter,
                'barcode' : "(t.barcode LIKE '%s')" % product_barcode
            }
        )

        return self._api_get(uri)


    def get_id_new_product(self, filter):
        raise NotImplementedError()



    def update_product(
        self,
        product_id:int,
        product_updated_data:dict,
    ) -> bool:
     
        uri = self.dolibarr_uri['product'] + str(product_id)
        return self._api_put(uri, product_updated_data)


    def delete_product(
        self,
        product_id:int,
    ) -> bool:
        
        raise NotImplementedError()


    def get_product_picture(
        self,
        product_id:int,
    ) -> str:

        try:
            uri = self.dolibarr_uri['documents'] + "?modulepart=product&id=%d" % product_id
            product_file_list = self._api_get(uri)

            selected_file = None
            for product_file in product_file_list:
                # As soon as we find an image -> stop
                if product_file['name'].lower().endswith(('.png', '.jpg', '.jpeg', '.gif')):
                    selected_file = product_file
                    break

            if selected_file == None:
                return ""

            uri = self.dolibarr_uri['documents']
            uri += "download?" 
            uri += "module_part=product&"
            uri += "original_file=%s/%s" % (selected_file['level1name'], selected_file['relativename'])

            return self._api_get(uri)['content']

        except Exception as err:
            return err



    def get_category_external_ref(
        self,
    ) -> dict:
        
        raise NotImplementedError()





    def get_cooperator(
        self,
        is_active:bool                = True,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:


        if is_active:
            is_active_filter = "(t.statut='1')"
        else:
            is_active_filter = "(t.statut='0')"        

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['members'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
            additionnals_sql_filters = {'is_active' : is_active_filter}
        )

        return self._api_get(uri)


    def import_cooperator(
        self,
        import_file:str,
    ) -> int:
    
        raise NotImplementedError()
    

    def get_invoice(
        self,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
            

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['invoices'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
        )

        # GET RESPONSE

        response = self._api_get(uri)
        nb_invoices = len(response)

        if nb_invoices < 1:
            raise ValueError("Nombre de factures inférieur à 1 : %d" % nb_invoices)

        return response


    def get_supplier_invoice(
        self,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
    

        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['supplier_invoices'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
        )
        
        # GET RESPONSE

        response = self._api_get(uri)
        nb_invoices = len(response)

        if nb_invoices < 1:
            raise ValueError("Nombre de factures inférieur à 1 : %d" % nb_invoices)

        return response




    def get_supplier(
        self,
        get_limit:int                 = None,
        sort_order:str                = None,
        created_after_date:datetime   = None,
        created_before_date:datetime  = None,        
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
    
        uri = self._create_uri(
            module_uri               = self.dolibarr_uri['suppliers'],
            get_limit                = get_limit,
            sort_order               = sort_order,
            created_after_date       = created_after_date,
            created_before_date      = created_before_date,
            modified_after_date      = modified_after_date,
            modified_before_date     = modified_before_date,
        )

        # GET RESPONSE

        response = self._api_get(uri)
        nb_supplier = len(response)

        if nb_supplier < 1:
            raise ValueError("Nombre de fournisseurs inférieur à 1 : %d" % nb_supplier)

        return response


    def get_supplier_by_id(
        self,
        supplier_id:int,
    ) -> dict:
        
        uri = self.dolibarr_uri['suppliers'] + str(supplier_id)
        response = self._api_get(uri)

        return response


    def get_supplier_by_product_id(
        self,
        product_id:int,
    ) -> dict:
     
        uri_last_product_movement = self._create_uri(
            module_uri                   = self.dolibarr_uri['stock_movements'],
            additionnals_param           = {'sortfield' : 't.tms'},
            sort_order                   = 'DESC',
            get_limit                    = 1,
            dolibarr_created_date_field  = 'datem',
            dolibarr_modified_date_field = 'tms',
            additionnals_sql_filters     = {'product_fk' : "(t.fk_product=%d)" % product_id},
        )

        try:
            last_product_movement = self._api_get(uri_last_product_movement)

        except ValueError:
            raise Exception("Unable to get supplier")


        supplier_id = last_product_movement[0]['fk_user_author']

        response = self.get_supplier_by_id(supplier_id)

        return response



    def get_stock_statut(
        self,
        dispatch_by_date:bool = False,
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> dict:
     
        raise NotImplementedError()


    def get_stock_movement(
        self,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:
    
        raise NotImplementedError()


    def get_date_of_last_stock_movement(
        self,
    ) -> datetime:

        raise NotImplementedError()


    def set_stock_movement_by_product_id(
        self,
        product_id:int,
        qty:int,
        movement_label:str,
    ) -> int:
    
        raise NotImplementedError()



    def get_account_by_code(
        self,
        account_code:int,
    ) -> dict:

        raise NotImplementedError()


    def get_account_record(
        self,
        fields:dict = None,
        detail:bool = False,        
        created_after_date:datetime  = None,
        created_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def add_account(
        self,
        acound_code:int,
        account_name:str,
    ) -> int:

        raise NotImplementedError()


    def get_bank_account_line(
        self,
        account_id:int = 1,
        modified_after_date:datetime  = None,
        modified_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_date_of_last_bank_account_auto_entry(
        self,
        account_id:int = 1,
    ) -> datetime:
     
        raise NotImplementedError()

    # def add_bank_account_auto_entry(
    #   self,
    #   type:PaymentType,
    #   label:str,
    #   date:datetime,
    #   amount:float,
    #   account_id:int = 1
    # ) -> int:
        
    #    raise NotImplementedError()


    def get_account_tax(
        self,
        fields:dict = None,
    ) -> List[dict]:

        raise NotImplementedError()


    def get_pos_sale_statement(
        self,
        stop_after_date:datetime  = None,
        stop_before_date:datetime = None,
    ) -> List[dict]:

        raise NotImplementedError()


    # def withdraw_option(
    #     self,
    #     obj_product,
    #     option_name
    # ):
    
    #     raise NotImplementedError()

    # def withdrawPrixKG(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawPrixL(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawWeightUnits(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawVolumeUnits(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesure(self, objProduit):
    #      raise NotImplementedError()

    # def withdrawMainMesure(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesureUnite(self, objProduit):
    #     raise NotImplementedError()

    # def withdrawMesurePrice(self, objProduit):
    #     raise NotImplementedError()





    # def floatToString(self, value):
    #     raise NotImplementedError()

    # def isFleg(self, productArray):
    #     raise NotImplementedError()

    # def getDicTVA(self):
    #     raise NotImplementedError()

    # def getDicSupplierTVA(self):
    #     raise NotImplementedError()