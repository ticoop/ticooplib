from .cloud_api import CloudApi
from .nextcloud_api import NextcloudApi

class Cloud():
    def __init__(
        self,
        cloud_name:str,
        cloud_base_url:str,
        cloud_username:str,
        cloud_password:str,
    ):

        self.cloud_name = cloud_name.lower()

        if self.cloud_name == 'nextcloud':
            self.api = NextcloudApi(
                cloud_base_url,
                cloud_username,
                cloud_password,
            )

        else:
            raise AttributeError("Invalid cloud_name, must be Nextcloud (no one else is implemented")