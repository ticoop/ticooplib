class CloudApi():

    def __init__(
        self,
        base_url:str,
        username:str,
        password:str,
    ):

        raise NotImplementedError()

    def upload_file(
        self,
        source_path:str,
        uri_target:str,
    ):

        raise NotImplementedError()