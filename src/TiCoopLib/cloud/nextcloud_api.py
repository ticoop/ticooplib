#!/usr/bin/python
#
#Version 1.0 - 2020-04-03 - Cyril CARGOU - Creation de la lib
#

#https://docs.nextcloud.com/server/15/developer_manual/client_apis/WebDAV/basic.html

from .cloud_api import CloudApi

#import du service client REST API
import os


class NextcloudApi(CloudApi):

    def __init__(
        self,
        base_url:str,
        username:str,
        password:str,
    ):

        self.base_url = base_url
        if not self.base_url.endswith("/"):
            self.base_url += "/"
        self.base_url += "remote.php/webdav"
        
        self.username = username
        self.password = password

    def _api(
        self,
        uri:str,
        params:str
    ):

        if uri.startswith("/"):
            uri = uri[1:len(uri)]

        return os.popen("curl -u %s:%s %s/%s %s" % (self.username, self.password, self.base_url, uri, params))

    def get_file(
        self,
        uri_target:str,
        dest_path:str
    ):
        std_in = self._api(uri_target, "--output %s" % dest_path) 
        # os.popen("curl -u %s:%s %s/%s --output %s" % (self.username, self.password, self.base_url, uri_target, dest_path))

        std_out = std_in.read()

    #par défaut mettre un / à la fin de l'urlTarget
    def upload_file(
        self,
        source_path:str,
        uri_target:str,
    ):

        if not os.path.isfile(source_path):
            raise FileNotFoundError("The file does not exist : "+ source_path)

        # Complète l'URI avec le nom du fichier si non présent (le fait par defaut mais à quelque problème avec les noms long)
        if uri_target.endswith('/') :
            uri_target += os.path.basename(source_path) 

        # std_in = os.popen("curl -u %s:%s -T %s %s/%s" % (self.username, self.password, source_path, self.base_url, uri_target))
        std_in = self._api(uri_target, "-T %s" % source_path)
    
        std_out = std_in.read()
        if len(std_out) == 0 :
            return True

        if std_out == "201":
            #logger.info('Upload fait - ajoute du fichier')
            return 1

        if std_out == "202":
            #logger.info('Upload fait - mise à jour du fichier')
            return 2

        raise Exception("Upload en echec, code retour : " + std_out)
        # si code 412, vérifier que la page wiki cible n'est pas en cours d'édition