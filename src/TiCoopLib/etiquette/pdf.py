#pip3 install reportlab
#pip3 install python-barcode

import sys, math,os

from reportlab.pdfgen import canvas
from reportlab.lib import pagesizes, units, colors, styles
from reportlab.pdfbase import pdfmetrics, ttfonts
from reportlab.platypus import Paragraph

import barcode
from barcode.writer import ImageWriter
from reportlab.lib.utils import ImageReader

import tempfile


#IMPORTANT : 
# x0,y0 = bottom left of the page
# X = axe verical
# Y = axe horizontal

"""
#Basé sur Code logiciel L4737 - Etiquettes multi-usages, 63,5 x 29,6 mm, 27 par feuille
#https://www.avery.fr/modele-l4737
labelWidth = 63.5 * units.mm
labelHeight = 29.6 * units.mm
marginPageLeft = 7.2 * units.mm #même chose à gauche
marginPageBottom = 15.1 * units.mm  #même chose en haut
marginLabelLeft= 2 * units.mm
marginLabelBottom= 0.1 * units.mm
#nombre d'étiquette par par ligne
nbLabelPerLine = 3
#nombre d'étiquette par colone
nbLabelPerColumn = 9
"""
#Basé sur EAL011 - Etiquettes multi-usages,70 x 37 mm, 24 par feuille
#http://www.europe100.eu/labels.html
labelWidth = 70 * units.mm
labelHeight = 37 * units.mm #+ 2 *  units.mm #2 min pour coller au papier
marginPageLeft = 0 * units.mm #même chose à gauche
marginPageBottom = 0 * units.mm  #même chose en haut
marginLabelLeft= 0 * units.mm
marginLabelBottom= 0 * units.mm
#nombre d'étiquette par par ligne
nbLabelPerLine = 3
#nombre d'étiquette par colone
nbLabelPerColumn = 8


paddingLabel= 4 * units.mm


#Import des polices
currentFolder = os.path.dirname(__file__)
pdfmetrics.registerFont(ttfonts.TTFont('TiCoopBold', currentFolder+'/Montserrat-Bold.ttf'))
pdfmetrics.registerFont(ttfonts.TTFont('TiCoopRegular', currentFolder+'/Montserrat-Regular.ttf'))
pdfmetrics.registerFont(ttfonts.TTFont('TiCoopItalique', currentFolder+'/Montserrat-Italic.ttf'))
pdfmetrics.registerFont(ttfonts.TTFont('TiCoopLightItalic', currentFolder+'/Montserrat-LightItalic.ttf'))


#retour un tabfleau de chaine découpée au Xeme mots (spération par un espace)
#si nombre négatif, départ depis la fin
def splitSentence(text,nbWordWhereSplit):
    separator = " "
    #si pas de séparation retourne la ligne
    if nbWordWhereSplit == 0:
        return [text,'']

    words = text.split(separator)
    nbWords = len(words)
    if nbWordWhereSplit < 0 :
        nbWordWhereSplit = nbWords + nbWordWhereSplit

    #Ligne 1
    strLine1 =""
    i = 0
    while i < nbWordWhereSplit and i < nbWords:
        strLine1 = strLine1 + separator + words[i]
        i = i + 1

    #Ligne 2
    strLine2 =""
    while i < nbWords:
        strLine2 = strLine2 + separator + words[i]
        i = i + 1

    return [strLine1.strip(),strLine2.strip()]

#Ecriture d'un text avec découpage de la phrase sur deux lignes si besoin
def writeText(textobject, text,nbLine = 1):
    global labelWidth, paddingLabel
    maxWidth = labelWidth - 2 * paddingLabel
    currentText = text
    currentLine = 0

    while currentText != '' :
        i = 0
        #trouve où découper la phrase
        while pdfmetrics.stringWidth(splitSentence(currentText,-i)[0],textobject._fontname, textobject._fontsize) > maxWidth :
            i = i + 1
        #découpe la phrase
        lines = splitSentence(currentText,-i)
        textobject.textLine(lines[0])
        currentLine = currentLine + 1
        #si la nomre de ligne max est atteint
        if currentLine >= nbLine :
            break
        #sinon, boucle sur le reste de la phrase 
        currentText = lines[1]


#Creation d'une etiquette
#un objet par ligne à écrire
#data = [{
#   "text" : <un text>,
#   "NbLigne" : su combien de ligne,
#   "Font" : TiCoopRegular | TiCoopBold | TiCoopItalique,
#   "Font-Size" : int,
#
# }]
def drawLabel(c,x,y,data):
    global labelWidth, labelHeight, paddingLabel
    # dessine le contour de l'étiquette en gris
    #c.setStrokeColor(colors.gray)
    c.setStrokeColorRGB(0.9, 0.9, 0.9)
    c.rect(x,y,labelWidth,labelHeight)
    
    textobject = c.beginText()
    #positionnement en haut à gauche du rectangle 
    # 2 * paddingLabel pour y car paddning haut et bas
    textobject.setTextOrigin(x+paddingLabel, y + labelHeight - 2*paddingLabel)
    #ecriture du text
    for l in data:
        textobject.setFont(l['Font'], int(l['Font-Size']))
        writeText(textobject,str(l['Text']),int(l['NbLigne']))
    
    #Prix TTC
    #textobject.setFont("TiCoopBold", 15)
    #strPrix = str(data['prixTTC']) + " €"
    #w = pdfmetrics.stringWidth(strPrix,textobject._fontname, textobject._fontsize)
    #positionnment en bas à droite
    #textobject.setTextOrigin(x + labelWidth - w - paddingLabel , y + paddingLabel)
    #textobject.textLine(strPrix)
    c.drawText(textobject)

#Calcul les coordonées X, Y de l'étiquette en foncton de son numéro 
#de 1 à 27
def getLabelOriginByNumber(numLabel):
    global nbLabelPerLine, nbLabelPerColumn,labelHeight, labelWidth, marginPageLeft, marginPageBottom,marginLabelLeft,marginLabelBottom

    #identification de la ligne et la colonne
    lineNumber = math.ceil(numLabel / nbLabelPerLine)
    colNumber = numLabel % nbLabelPerLine
    #print(colNumber)
    if colNumber == 0 : colNumber = nbLabelPerLine
   
    #caclul des positions
    y = marginPageBottom + ((lineNumber-1) * labelHeight) + ((lineNumber -1 ) * marginLabelBottom)
    x = marginPageLeft   + ((colNumber -1)  * labelWidth) + ((colNumber  -1 ) * marginLabelLeft)
    #print ([lineNumber,colNumber,x,y])
    return [x,y]



#Fonctino de création d'un image avec le code barre demandé
def createBarCode(number,type='EAN13'):
    EAN = barcode.get_barcode_class(type)
    ean = EAN(number, writer=ImageWriter())
    fileName =tempfile.NamedTemporaryFile(prefix='ena13-').name
    #https://python-barcode.readthedocs.io/en/latest/writers/
    return ean.save(fileName,options={"text_distance": 1,"quiet_zone":2})
    #return barcode.generate('EAN13', number, writer=ImageWriter(), output='barcode')
     

def createBarCodeEAN13(number):
    return  createBarCode(str(number),'EAN13')
#pixels
def drawPicture(canvas,filePath,x,y,width=10,height=10,):
    img = ImageReader(filePath)
    #pixels
    canvas.drawImage(img,x=x,y=y,width=width,height=height)