#!/usr/bin/python
#
#Version 1.0 - 2020-04-19 - Cyril CARGOU - Lib de fonctions génériques et/ou preconfiguré
#

#import du service de log pour tracer l'action du script
import logging
#import du service de gestion des chemins 
import os,sys, datetime
# import du service de regex
import re

#----------------Log 
GLOBAL_LOG_LOGGER = None

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def removeCR(str):
    return str.replace('\n', ' ').replace('\r', '')

def checkRegex(inputStr, regex):
    # compiling the pattern for alphanumeric string
    pat = re.compile(regex)

    # Checks whether the whole string matches the re.pattern or not
    return re.fullmatch(pat, inputStr)



def logOpen(logFolder = '/var/log/ticoop/crontab'):
    global GLOBAL_LOG_LOGGER
    if GLOBAL_LOG_LOGGER == None:
        #recuperation du dossier courant
        scriptName = getScriptName()
        fileDate = datetime.datetime.today().strftime("%Y-%m-%d_%H%M%S")
        #ouverture du log
        GLOBAL_LOG_LOGGER = logging.getLogger(scriptName)
        hdlr = logging.FileHandler(logFolder+"/"+fileDate+"_"+scriptName+'.log')
        hdlr.setFormatter(logging.Formatter('%(asctime)s ; %(levelname)s ; %(message)s'))
        GLOBAL_LOG_LOGGER.addHandler(hdlr) 
        #GLOBAL_LOG_LOGGER.setLevel(logging.WARNING)#niveau de log voulu (INFO / WARNING / ERROR)
        #GLOBAL_LOG_LOGGER.setLevel(logging.INFO)#niveau de log voulu (INFO / WARNING / ERROR)
        GLOBAL_LOG_LOGGER.setLevel(logging.DEBUG)#niveau de log voulu (INFO / WARNING / ERROR)

    logInfo('---------------------------------')
    logInfo('Lancement du script :' + getScriptFullPath())

def logClose():
    global GLOBAL_LOG_LOGGER
    GLOBAL_LOG_LOGGER.setLevel(logging.INFO)#pour indiquer que le script est OK
    logInfo('Fin du script : OK')
def logCloseAndExit():
    logClose()
    exit(0)
def logCloseOnError(error=""):
    logError('Fin du script : ERREUR -> ' + error)
def logCloseAndExitOnError(error=""):
    logCloseOnError(error)
    exit(1)

def logDebug(msg):
    return log(msg,logging.DEBUG)
def logInfo(msg):
    return log(msg,logging.INFO)
def logWarning(msg):
    return log(msg,logging.WARNING)
def logError(msg):
    return log(msg,logging.ERROR)
def log(msg:str,type=logging.INFO):
    global GLOBAL_LOG_LOGGER
    print (msg)
    if type == logging.ERROR: GLOBAL_LOG_LOGGER.error(msg)
    elif type == logging.WARNING: GLOBAL_LOG_LOGGER.warning(msg)
    elif type == logging.INFO: GLOBAL_LOG_LOGGER.info(msg)
    elif type == logging.DEBUG: GLOBAL_LOG_LOGGER.debug(msg)
    else : GLOBAL_LOG_LOGGER.info(msg)

def getScriptFullPath():
    #scriptName = os.path.basename(__file__)
    return os.path.abspath(sys.argv[0])
def getScriptDirPath():
    #scriptName = os.path.basename(__file__)
    return os.path.abspath(os.path.dirname(getScriptFullPath()))
def getScriptName():
    #scriptName = os.path.basename(__file__)
    return os.path.basename(getScriptFullPath())


def run(cmd,context=None):
    #su  admin -c commande
    if context == None : 
        su = ""
    else:
        su = "su "+context+" -c "
    stdout = os.popen(su+cmd)
    return stdout.read()

#password doit être chiffré : echo -n 'password' | md5sum
def createUser(login,password=None,isAdmin=False):
    #recherche de l'utilisateur
    if int(run("getent passwd "+login+"| wc -l ")) < 1:
        #non trouvé, le créer
        run("useradd "+login)

    if password == None:
        run("passwd --delete "+login)
        run("passwd --force --unlock "+login)
    else:
        run("echo '"+login+":21232f297a57a5a743894a0e4a801fc3' | chpasswd --encrypted")
    
    if isAdmin == True:
        #donne le droit admin
        run("adduser --group sudo "+login)
    
    return True

def setGsettings(objet, attribut, value,context=None):
    print("gsettings set "+objet+" " +attribut+" " +str(value))
    return True
    #return run("gsettings set "+objet+" " +attribut+" " +str(value),context)

def makeDesktopShortcut(path,name,target,iconPath=None):
    print(path)
    shortcut = open(path, 'w')
    shortcut.write('[Desktop Entry]')
    shortcut.write('Encoding=UTF-8')
    shortcut.write("Name="+name)
    shortcut.write("Type=Link")
    shortcut.write("URL="+target)
    if iconPath != None : 
        shortcut.write("Icon="+iconPath)
    shortcut.close()

#fonction qui recherche un parametre dans les argument de lancement du script
#uniquement des valeur compelte type --paramName ou --paramName=
#valeur de paramName : paramName ou paramName=
def getScriptParameter(paramName,defaultValue):
    #non utilisation de getopt car il faut que tout les arguments soit déclaré en même temps....
    #try:
        #: et = indiquent qu'une valeur est attendue
    #    opts, args = getopt.getopt(sys.argv[1:],"",[paramName])
    #except getopt.GetoptError:
    #    return defaultValue
    #print(sys.argv[1:]) #['--day=10','--whatIf ']
    
    #parcour la liste des arguments
    for a in sys.argv[1:]:
        #si --day= ou --whatIf est dans le tableau
        if a.startswith("--"+paramName) == True:
            #argument trouvé, test si argument swtich
            if paramName.find("=") > -1:
                #ce n'set pas un switch
                return  a.split("=")[1]
            else:
                # c'est un swtich
                return True
    #si non trouvé, retour la valeur par défaut               
    return defaultValue
