#!/usr/bin/python
#
#Version 1.0 - 2020-05-10 - Cyril CARGOU - Lib d'outil de gestion des codes-barre
#
#python3 ./script.py


def getEAN13Checksum(strCode):
    """ Compute the checksum of bar code """
   
    # UPCA/EAN13
    weight=[1,3]*6
    magic=10
    sum = 0
    
    for i in range(12):         # checksum based on first 12 digits.
        sum = sum + int(strCode[i]) * weight[i]
    z = ( magic - (sum % magic) ) % magic
    if z < 0 or z >= magic:
        return None
    return z

#fonction qui 
def getEAN13Normalized(strCode):
    #si déja bonne longeur, pas d'action
    intLen = len(strCode)
    if intLen == 13:
        return strCode

    #si trop court, complément avec des 0 à la fin 
    if intLen < 12:
        intNbMissing = 12 - intLen
        for i in range(intNbMissing):
            strCode = strCode + "0"
    #si trop long découpe 
    else: 
        if intLen > 12:
            strCode=strCode[0:12]

    #la taille des de 12, ajout de la lef
    return strCode + str(getEAN13Checksum(strCode))
