#!/usr/bin/python
#
#Version 1.0 - 2020-06-03 - Cyril CARGOU - Creation de la lib
#

#import du service client REST API
import os
import requests
from requests.auth import HTTPBasicAuth
import json

#------------- Variables ------------------
username = 'TO_BE_SET'
password = 'TO_BE_SET'
baseURL = 'TO_BE_SET'
apiKey = None #setBy_getTocken', By default this token expires in 1 minute

#fonction de demande d'un token pour la session de travail
def _getTocken():
    global username,password
    #https://github.com/keycloak/keycloak-documentation/blob/master/server_development/topics/admin-rest-api.adoc
    data = {
        "client_id"  : "admin-cli",
        "username" : username,
        "password" : password,
        "grant_type" : "password"
        }
    #data = urlencode(data).encode()
    url = "https://my.ticoop.fr/auth/realms/ticoop/protocol/openid-connect/token"
    headers = {}
    headers['Accept']           = 'application/json'
    headers['Content-Type']     = 'application/x-www-form-urlencoded'
    response = requests.post(url, headers=headers, data = data)
    
    if response.status_code != 200:
        #raise Exception('_getTocken : ' + str(response.status_code) + " URL : " + url)
        response.raise_for_status()
  
    return response.json()['access_token']

#Fonction prive de requetes API
def _apiGet(URI):
    return _api(URI, 'GET')

#Fonction prive de requetes API
def _apiPost(URI,data):
    return _api(URI, 'POST',data)

#Fonction prive de requetes API
def _apiPut(URI,data):
    return _api(URI, 'PUT',json.dumps(data))

#Fonction prive de requetes API
def _api(URI,type='GET',data=None):
    global baseURL, apiKey

    if apiKey == None: apiKey =  _getTocken()

    headers = {}
    headers['Authorization']    = 'Bearer '+apiKey
    headers['Accept']           = 'application/json'

    url = baseURL  + URI

    if  type == 'GET':
        response = requests.get(url, headers=headers)
    elif type == 'POST':
        response = requests.post(url, headers=headers, data = data)
    elif type == 'PUT':
        headers['Accept'] = 'application/json'
        headers['Content-Type'] = 'application/json'
        response = requests.put(url, headers=headers, data = data)
    else:
        raise Exception('_api request type unknow : ' + type)
    #print(response)
    #print(response.headers)
    #print(response.text)
    #print(response.content)
    #print(response.encoding)
    #response.raise_for_status()

    if response.status_code == 204:
        return
    if response.status_code != 200:
        raise Exception('_api iam : ' + str(response.status_code) + " URL : " + url)

    return response.json()

def getCooperator():
    return _apiGet("/users?max=3000")

def getActiveCooperator():
    cooperatorList = getCooperator()
    out = []

    for c in cooperatorList:
        if c['enabled'] == True and not c['username'].startswith("service."):
            out.append({
                'id' : c['id'],
                'username' : c['username'],
                'email' : c['email'],
            })

    return out

def disableCooperator(id):
    _apiPut("/users/"+id,{"enabled":False})