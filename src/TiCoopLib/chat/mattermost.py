#!/usr/bin/python

#import du service de gestion des chemins 
import os 
#import du service client REST API
import requests
from requests.auth import HTTPBasicAuth
import json
#pour la gestion des accents
import unicodedata

#------------- Variables ------------------
#Token de connexion à Mattermost
apiKey = 'TODO'
baseURL = 'TODO'

#Fonction prive de requetes API
def _apiGet(URI):
    return _api(URI, 'GET')

#Fonction prive de requetes API
def _apiPost(URI,data):
    return _api(URI, 'POST',data)

def _apiPut(URI,data):
    return _api(URI, 'PUT',data)

def _apiDelete(URI):
    return _api(URI, 'DELETE')

#Fonction prive de requetes API
def _api(URI,type='GET',data=None):
    global baseURL, apiKey
   
    headers = {'Authorization': 'Bearer '+apiKey}
    url = baseURL  + URI

    if  type == 'GET':
        response = requests.get(url, headers=headers)
    elif type == 'POST':
        response = requests.post(url, headers=headers, data = data)
    elif type == 'PUT':
        #headers['Accept'] = 'application/json'
        #headers['Content-Type'] = 'application/json'
        response = requests.put(url, headers=headers, data = data)
    elif type == 'DELETE':
        response = requests.delete(url, headers=headers)
    else:
        raise Exception('_api request type unknow : ' + type)
   
    if response.status_code == 404:#pas de data
        return []
    if response.status_code != 200:
        raise Exception('_api status_code : ' + str(response.status_code) + " URL : " + url)

    return response.json()


def testConnexion(username = None):
    try:
        response = _apiGet('/api/v4/users/me')
        if response['success']['code'] != 200:
            return False
        if username != None and response['username'] != username:
            return False
        return True
    except Exception:
        return False

def getTeams(exludeTeam=None,includeMember=True):
    response = _apiGet('/api/v4/teams')
    
    #mise en forme
    equipeArray = {}
    for e in response:
        #logger.info('Mise en tableau de : ' + e['display_name'] + ' => ' + e['id'])
        #exlusion de team de la listeame
        teamName = e['display_name']
        if teamName in exludeTeam: 
            continue
        equipeArray[teamName] = {"id" : e['id'], "membre" : {}}
        if includeMember:
            equipeArray[teamName]["membre"] = getTeamMembers(e['id'])

    return equipeArray

def getTeamMembers(idTeam: str, sortArray = True):
  
    #Préparation de la boule de gestion des pages de retours (max 200 membres)
    idPage = -1
    nbUser = 200 #pour entrer dans la boucle
    tmpArray = {}#tableau de mémorisation des utilisateurs
    while nbUser >= 200:
        idPage = idPage + 1
        url = '/api/v4/users?in_team='+idTeam+'&per_page=200&page='+str(idPage)
        userList = _apiGet(url)
        nbUser = len(userList)
        #mise en forme des utilisateurs
        for u in userList:
            #suppression du compte admin
            if u['username'] == "admin":
                continue
            #intégration du droit sur l'équipe (attention de l'équipe, pas de l'utilisateur)
            #cette api ne retour que system_admin ou system_user
            #if u['roles'].find("team_admin") != -1:
            #    role = "Administration d'equipe"#  "roles":"team_user team_admin"
            #else:
            role = "Utilisateur"
                
            #logger.info(nomEquipe + " : " + u['id'] + " => " + u['username'])
            #utilisation d'une clef double prénomnom afin de permettre un trie correcte
            key = (u['first_name']+u['last_name']).lower()
            #supprime les accents
            key=unicodedata.normalize('NFD', key).encode('ascii', 'ignore').decode('ascii')
            tmpArray[key] = {"nom" : u['last_name'].upper(),"prenom" : u['first_name'],  "role" : role, "login" : u['username'] }#"mail" : u['email'],

    if sortArray :
        #mise en forme afin de trier les utilisateurs
        #Récupération de la liste des clefs
        tmpArrayKeys = [ k for k in tmpArray ]
        tmpArrayKeys = sorted(tmpArrayKeys)
        sortedArray ={}
        for u in tmpArrayKeys:
            sortedArray[u] = tmpArray[u]
        return sortedArray
    return  tmpArray

def getActiveCooperator():
    #Préparation de la boule de gestion des pages de retours (max 200 membres)
    idPage = -1
    nbUser = 200 #pour entrer dans la boucle
    tmpArray = [] #tableau de mémorisation des utilisateurs
    while nbUser >= 200:
        idPage = idPage + 1
        url = '/api/v4/users?per_page=200&page='+str(idPage)
        userList = _apiGet(url)
        nbUser = len(userList)
        #mise en forme des utilisateurs
        for u in userList:
            #conservation que des comptes actifs
            if u['delete_at'] != 0: continue
            #suppression du compte admin
            if u['username'] == "admin": continue
            print(u)
            exit(0)
            tmpArray.append({"last_name" : u['last_name'],
                            "first_name" : u['first_name'], 
                            "username" : u['username'],
                            "email" : u['email'],
                            "id" : u['id']
                            })
    return  tmpArray

def disableCooperator(id):
    _apiDelete("/users/"+id)
