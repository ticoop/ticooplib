# TiCoopLib

This library provides high functions frequently use by Ti Coop (VFLeg, scripts cron, surcouche-erp, ...)


## Install
`pip install dist/TiCoopLib-*.whl`

## Use
`from TiCoopLib.Util import util`

## Developers
### Build a wheel
`python3 -m build`

### Send to PyPi (Python Package Index)
PyPi is the official third-party software repository for Python. Pip is by default connect to his URL

We use twine to upload release. Twine except a API token, we have to create an account and a API token on instance.
The token can be set in $HOMEDIR/.pypirc file :
```
[distutils]
  index-servers =
    testpypi
    TiCoopLib

[testpypi]
  username = __token__
  password = {tokenValue}

[TiCoopLib]
  repository = https://test.pypi.org/legacy/
  username = __token__
  password = {tokenValue}
```

[Full documentation](https://packaging.python.org/en/latest/tutorials/packaging-projects/)

#### Send to TestPyPi
`python3 -m twine upload --repository testpypi dist/*`

#### Send to PyPi
`python3 -m twine upload dist/*`

### Install
#### Install from wheel
`pip install dist/TiCoopLib-*.whl`

#### Install from TestPyPi
`pip install --index-url https://test.pypi.org/simple/ TiCoopLib`

(Note that python-barcode isn't on TestPyPi and may generate a dependency error. For fix it, the simpliest way is to install it : `pip install python-barcode` before install TiCoopLib)

#### Install from PyPi
`pip install TiCoopLib`